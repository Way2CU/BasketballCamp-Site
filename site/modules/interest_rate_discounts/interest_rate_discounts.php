<?php
/**
 * Module providing different interesate rate "discounts". These are negative
 * discounts which increase value of final payment. This is due to credit card
 * company being unwilling to allow for such services to be charged from buyer
 * on virtual goods.
 *
 * Author: Mladen Mijatov
 */

use Core\Events;
use Core\Module;


class interest_rate_discounts extends Module {
	private static $_instance;
	private $promotion_6;
	private $promotion_10;

	/**
	 * Constructor
	 */
	protected function __construct() {
		parent::__construct(__FILE__);

		if (ModuleHandler::is_loaded('shop')) {
			require_once('units/discounts.php');
			$this->promotion_6 = new Installment6Payment($this);
			$this->promotion_10 = new Installment10Payment($this);

			$shop = shop::get_instance();
			$shop->registerPromotion($this->promotion_6);
			$shop->registerPromotion($this->promotion_10);

			if (ModuleHandler::is_loaded('cardcom')) {
				$cardcom = cardcom::get_instance();
				$cardcom->set_promotion_max_payments($this->promotion_6->get_name(), 6);
				$cardcom->set_promotion_max_payments($this->promotion_10->get_name(), 10);
			}
		}
	}

	/**
	 * Public function that creates a single instance
	 */
	public static function get_instance() {
		if (!isset(self::$_instance))
		self::$_instance = new self();
		return self::$_instance;
	}

	/**
	 * Transfers control to module functions
	 *
	 * @param array $params
	 * @param array $children
	 */
	public function transfer_control($params = array(), $children = array()) {
	}

	/**
	 * Event triggered upon module initialization
	 */
	public function initialize() {
	}

	/**
	 * Event triggered upon module deinitialization
	 */
	public function cleanup() {
	}
}
