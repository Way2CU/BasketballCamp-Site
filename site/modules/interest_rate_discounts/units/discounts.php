<?php

/**
 * Shop discounts depending on number of payments selected.
 */

use Modules\Shop\Promotion\Discount;
use Modules\Shop\Promotion\Promotion;


class Installment6Payment extends Promotion {
	private $parent;
	private $discount;

	public function __construct($parent) {
		$this->name = 'payment-6-installments';
		$this->parent = $parent;

		$this->discount = new Surcharge5($parent);

		$shop = shop::get_instance();
		$shop->registerDiscount($this->discount);
	}

	/**
	 * Get multi-language name for the promotion. This name
	 * is used for showing user applied promotion instead of using
	 * unique strings.
	 *
	 * @return string
	 */
	public function get_title() {
		return $this->parent->get_language_constant('installments_3to6');
	}

	/**
 	 * Check if specified transaction qualified for this promotion.
	 *
	 * @return boolean
	 */
	public function qualifies() {
		$items = array_keys($_SESSION['shopping_cart']);
		return in_array('ticket', $items);
	}

	/**
	 * Return discount associated with this promotion. This object
	 * will specify the amount being deduced from final.
	 *
	 * @return object
	 */
	public function get_discount() {
		return $this->discount;
	}
}

class Installment10Payment extends Promotion {
	private $parent;
	private $discount;

	public function __construct($parent) {
		$this->name = 'payment-10-installments';
		$this->parent = $parent;

		$this->discount = new Surcharge10($parent);

		$shop = shop::get_instance();
		$shop->registerDiscount($this->discount);
	}

	/**
	 * Get multi-language name for the promotion. This name
	 * is used for showing user applied promotion instead of using
	 * unique strings.
	 *
	 * @return string
	 */
	public function get_title() {
		return $this->parent->get_language_constant('installments_7to10');
	}

	/**
 	 * Check if specified transaction qualified for this promotion.
	 *
	 * @return boolean
	 */
	public function qualifies() {
		$items = array_keys($_SESSION['shopping_cart']);
		return in_array('ticket', $items);
	}

	/**
	 * Return discount associated with this promotion. This object
	 * will specify the amount being deduced from final.
	 *
	 * @return object
	 */
	public function get_discount() {
		return $this->discount;
	}
}


class Surcharge5 extends Discount {
	private $parent;

	public function __construct($parent) {
		$this->name = 'surcharge-5-percent';
		$this->parent = $parent;
	}

	/**
	 * Get multi-language name for the discount. This name
	 * is used for showing user applied discount instead of using
	 * unique strings.
	 *
	 * @return string
	 */
	public function get_title() {
		return $this->parent->get_language_constant('discount_3to6');
	}

	/**
	 * Apply discount on specified trancation. Result is a list
	 * of discount items. These items do not reflect shop items in the
	 * cart. Instead they represent different deduction from the final
	 * price.
	 *
	 * @param object $transaction
	 * @return array
	 */
	public function apply($transaction) {
		$shop = shop::get_instance();
		$summary = $shop->getCartSummary(null, TransactionType::REGULAR);
		$surcharge = $summary['total'] * 0.05;

		return array(null, 1, -$surcharge);
	}
}


class Surcharge10 extends Discount {
	private $parent;

	public function __construct($parent) {
		$this->name = 'surcharge-10-percent';
		$this->parent = $parent;
	}

	/**
	 * Get multi-language name for the discount. This name
	 * is used for showing user applied discount instead of using
	 * unique strings.
	 *
	 * @return string
	 */
	public function get_title() {
		return $this->parent->get_language_constant('discount_7to10');
	}

	/**
	 * Apply discount on specified trancation. Result is a list
	 * of discount items. These items do not reflect shop items in the
	 * cart. Instead they represent different deduction from the final
	 * price.
	 *
	 * @param object $transaction
	 * @return array
	 */
	public function apply($transaction) {
		$shop = shop::get_instance();
		$summary = $shop->getCartSummary(null, TransactionType::REGULAR);
		$surcharge = $summary['total'] * 0.1;

		return array(null, 1, -$surcharge);
	}
}

?>
