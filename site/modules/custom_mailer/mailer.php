<?php

/**
 * Custom Caracal Mailer
 *
 * Author: Mladen Mijatov
 */
namespace Modules\CustomMailer;

use \ContactForm_Mailer;


class Mailer extends ContactForm_Mailer {
	private $variables = array();

	const NOTIFY_ENDPOINT = 'https://hook.eu1.make.com/xj4ce0dtghyzx6pv939u7em708we1rfa';

	/**
	 * Get localized name.
	 *
	 * @return string
	 */
	public function get_title() {
		return 'Make Webhook';
	}

	public function start_message() {}

	/**
	 * Finalize message and send it to specified addresses.
	 *
	 * Note: Before sending, you *must* check if contact_form
	 * function detectBots returns false.
	 *
	 * @return boolean
	 */
	public function send() {
		// prepare notification data
		$url = self::NOTIFY_ENDPOINT;
		$parameters = $this->variables;

		// send data to notification endpoint
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parameters));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$raw_response = curl_exec($ch);
		curl_close($ch);

		return true;
	}

	/**
	 * Set variables to be replaced in subject and body.
	 *
	 * @param array $params
	 */
	public function set_variables($variables) {
		$this->variables = $variables;
	}

	public function set_sender($address, $name=null) {}
	public function add_recipient($address, $name=null) {}
	public function add_cc_recipient($address, $name=null) {}
	public function add_bcc_recipient($address, $name=null) {}
	public function add_header_string($key, $value) {}
	public function set_subject($subject) {}
	public function set_body($plain_body, $html_body=null) {}
	public function attach_file($file_name, $attached_name=null, $inline=false) {}
}

?>
