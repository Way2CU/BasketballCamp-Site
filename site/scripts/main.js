/**
 * Main JavaScript
 * Site Name
 *
 * Copyright (c) 2018. by Way2CU, http://way2cu.com
 * Authors:
 */

// create or use existing site scope
var Site = Site || {};

// make sure variable cache exists
Site.variable_cache = Site.variable_cache || {};


/**
 * Check if site is being displayed on mobile.
 * @return boolean
 */
Site.is_mobile = function() {
	var result = false;

	// check for cached value
	if ('mobile_version' in Site.variable_cache) {
		result = Site.variable_cache['mobile_version'];

	} else {
		// detect if site is mobile
		var elements = document.getElementsByName('viewport');

		// check all tags and find `meta`
		for (var i=0, count=elements.length; i<count; i++) {
			var tag = elements[i];

			if (tag.tagName == 'META') {
				result = true;
				break;
			}
		}

		// cache value so next time we are faster
		Site.variable_cache['mobile_version'] = result;
	}

	return result;
};

/**
 * Handle clicking on tab for showing video.
 */
Site.handle_video_click = function(event) {
	if (!Site.video_container)
		return;

	// show and hide video container
	Site.video_container.classList.toggle('visible');

	if (Site.video_container.classList.contains('visible') && !Site.video_played) {
		Site.video_played = true;
		var link = Site.video_container.querySelector('a');
		link.click();
	}
};

Site.TicketView = function(item) {
	var self = this;

	self.item = item;
	self.cart = item.cart;
	self.currency = null;
	self.exchange_rate = 1;

	self.container = null;
	self.label_name = null;
	self.option_remove = null;

	/**
	 * Complete object initialization.
	 */
	self._init = function() {
		var template = $('div#form div.ticket.template');
		var list_container = self.cart.get_list_container();

		// clone template ticket and add it to the list
		self.container = template.clone();
		self.container.removeClass('template');

		self.label_name = self.container.find('strong');

		self.option_remove = self.container.find('button[name=remove]');
		self.option_remove.on('click', self._handle_remove);
		self.option_change = self.container.find('button[name=edit]');
		self.option_change.on('click', self._handle_change);

		self.container.insertBefore(Site.total_cost);
	}

	/**
	 * Handle clicking on remove item.
	 *
	 * @param object event
	 */
	self._handle_remove = function(event) {
		event.preventDefault();
		self.item.remove();
	};

	/**
	 * Handle clicking on change button. Since variation id depends on
	 * item properties only way to implement ability to change is to remove
	 * existing item and pre-populate fields in editor.
	 *
	 * @param object event
	 */
	self._handle_change = function(event) {
		event.preventDefault();
		Site.input_parent_name.value = self.item.properties['parent_name'];
		Site.input_parent_phone.value = self.item.properties['parent_phone'];
		Site.input_name.value = self.item.properties['name'];
		Site.input_phone.value = self.item.properties['phone'];
		Site.input_email.value = self.item.properties['email'];
		Site.input_personal_id.value = self.item.properties['personal_id'];
		Site.input_passport_number.value = self.item.properties['passport_number'];
		Site.input_club_name.value = self.item.properties['club_name'];
		Site.input_clothing_size.value = self.item.properties['clothing_size'];
		Site.input_name.focus();
		self.item.remove();
	};

	/**
	 * Handler externally called when item count has changed.
	 */
	self.handle_change = function() {
		self.label_name.text(self.item.properties['name']);
	};

	/**
	 * Handle shopping cart currency change.
	 *
	 * @param string currency
	 * @param float rate
	 */
	self.handle_currency_change = function(currency, rate) {
		// store values
		self.currency = currency;
		self.exchange_rate = rate;

		// update labels
		self.handle_change();
	};

	/**
	 * Handler externally called before item removal.
	 */
	self.handle_remove = function() {
		self.container.remove();
	};

	// finalize object
	self._init();
}

/**
 * Handle clicking on add ticket button. This function will create new
 * item in the shopping cart using properties like name, phone and email
 * for generating variation id.
 *
 * @param object event
 */
Site.add_ticket = function(event) {
	event.preventDefault();

	// collect properties for ticket
	var properties = {
		name: Site.input_name.value,
		email: Site.input_email.value,
		phone: Site.input_phone.value,
		personal_id: Site.input_personal_id.value,
		passport_number: Site.input_passport_number.value,
		club_name: Site.input_club_name.value,
		clothing_size: Site.input_clothing_size.value,
		parent_name: Site.input_parent_name.value,
		parent_phone: Site.input_parent_phone.value,
	};

	// make sure we have all the data
	var optional = ['club_name'];
	for (var key in properties) {
		if (properties[key] == '' && optional.indexOf(key) == -1)
			return;
	}

	// reset form and prepare for new entry
	Site.input_name.value = '';
	Site.input_email.value = '';
	Site.input_phone.value = '';
	Site.input_personal_id.value = '';
	Site.input_passport_number.value = '';
	Site.input_club_name.value = '';
	Site.input_clothing_size.value = '';
	Site.input_name.focus();

	var tickets = new Map([  // different tickets for different pages
		['/en', 'ticket-without-travel'],
		['/en/israel', 'ticket-israel'],
		['/israel', 'ticket-israel'],
	]);
	var path = location.pathname;
	if (path.endsWith('/'))
		path = path.substr(0, path.length - 1);

	if (tickets.has(path))
		Site.tickets.add_item_by_uid(tickets.get(path), properties); else
		Site.tickets.add_item_by_uid('ticket', properties);

	// change button text to alternative one
	event.target.childNodes[1].textContent = event.target.dataset['altText'];
};

/**
 * Show dialog for ticket entry after form has been submitted. Always returns
 * false to prevent system dialog from showing.
 *
 * @param object data
 * @return boolean
 */
Site.handle_form_submit = function(data) {
	// populate data in the dialog
	var forms = document.querySelectorAll('form');
	if (forms.length == 0)
		return true;

	for (var i=0, count=forms.length; i<count; i++) {
		var form = forms[i];
		var input_name = form.querySelector('input[name=name]');
		var phone_number = form.querySelector('input[name=phone]');

		if (input_name.value != '' || phone_number.value != '')
			break;
	}

	Site.input_parent_name.value = input_name.value;
	Site.input_parent_phone.value = phone_number.value;

	// finally show the dialog
	Site.dialog.open();

	return false;
};

/**
 * Function called when document and images have been completely loaded.
 */
Site.on_load = function() {
	if (Site.is_mobile()) {
		Site.mobile_menu = new Caracal.MobileMenu();

	} else {
		var video_container = document.querySelector('section#video');
		if (video_container) {
			Site.video_container = video_container;
			Site.video_tab = Site.video_container.querySelector('button');
			Site.video_played = false;

			setTimeout(function() { Site.video_container.classList.remove('visible'); }, 1000);
			Site.video_tab.addEventListener('click', Site.handle_video_click);
		}
	}

	// replace no promotion text
	if (location.pathname == '/checkout' || location.pathname == '/en/checkout') {
		Caracal.language.load_text(null, 'label_no_promotion', function(constant, data) {
			document.querySelector('div#qualified_promotions label:nth-of-type(1) span').innerText = data;
		});
	}

	// if form is present create and configure shopping cart and necessary
	// user interface elements to operate ticketing system
	if (document.querySelector('div#form')) {
		Site.dialog = new Caracal.Dialog(
					{ clear_on_close: false, include_close_button: true },
					{ close: 'close', title: 'call_to_action' }
				);
		Site.dialog.set_content_from_dom('div#form');
		Site.dialog.add_class('form');

		// create checkout button
		var button_checkout = document.querySelector('a.checkout');
		Site.dialog.add_control(button_checkout);

		// create totals label
		Site.total_cost = document.querySelector('div#form div.total');

		if (Site.is_mobile())
			Site.dialog.set_size('90vw', null); else
			Site.dialog.set_size('501px', null);

		// attach click handler for all call to actions
		for (var i=0, count=Caracal.ContactForm.list.length; i<count; i++) {
			var form = Caracal.ContactForm.list[i];
			form.events.connect('submit-success', Site.handle_form_submit);
		}

		// create shopping cart and attach elements
		Site.tickets = new Caracal.Shop.Cart();
		Site.tickets
			.set_checkout_url('/checkout')
			.add_item_view(Site.TicketView)
			.ui.add_item_list($('div#form div.ticket-list'))
			.ui.add_total_cost_label(Site.total_cost.querySelector('span.value'))
			.ui.connect_checkout_button(button_checkout);

		// connect button for adding new tickets
		Site.input_name = document.querySelector('div#form div.editor input[name=name]');
		Site.input_email = document.querySelector('div#form div.editor input[name=email]');
		Site.input_phone = document.querySelector('div#form div.editor input[name=phone]');
		Site.input_personal_id = document.querySelector('div#form div.editor input[name=personal_id]');
		Site.input_passport_number = document.querySelector('div#form div.editor input[name=passport_number]');
		Site.input_club_name = document.querySelector('div#form div.editor input[name=club_name]');
		Site.input_clothing_size = document.querySelector('div#form div.editor select[name=clothing_size]');
		Site.input_parent_name = document.querySelector('div#form div.editor input[name=parent_name]');
		Site.input_parent_phone = document.querySelector('div#form div.editor input[name=parent_phone]');
		Site.button_add = document.querySelector('div#form div.editor div.controls button[name=add]');

		Site.button_add.addEventListener('click',  Site.add_ticket);
	}
};


// connect document `load` event with handler function
window.addEventListener('load', Site.on_load);
